# K3ｓ環境整備サンプルファイル

/simple-test/  
K3sデプロイ後に動作確認をするためのnginxをデプロイするyaml


/cache-test/  
K3sでCacheを有効にした後にCacheの動作を確認するための.gitlab-ci.ymlファイル

/jhipster/  
JHipsterのアプリケーションの説明とデプロイ方法についてのyamlファイル

